'use strict';

module.exports = {
    port: 3000,
    hostname : '127.0.0.1',
    baseUrl : 'http://localhost:3000',
    mongodb : {
        uri : 'mongodb://lamar:123@ds131480.mlab.com:31480/mean_bprints_contact_manager'
    },
    app: {
        name: "Contact Manager"
    },
    severStatic : true,
    session : {
        type : 'mongo',
        secret : '29c9s8a8s8e90w00d9s88d88w889',
        resave : false, 
        saveUninitialized : true
    }
};