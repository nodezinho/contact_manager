"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema; 

var UserSchema = new Schema({
    email : {
        type : String
    }, 
    name : {
        type : String
    },
    city : {
        type : String
    },
    phoneNumber : {
        type : String
    },
    company : {
        type : String
    },
    createdAt : {
        type : Date, 
        default : Date.now
    }
});

module.exports = mongoose.model("User", UserSchema);
